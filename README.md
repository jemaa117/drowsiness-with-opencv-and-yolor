# Drowsiness Detection with OpenCV and YOLO-R

This repository contains two folders: 
- `drowsiness-opencv`: This folder contains code to detect drowsiness using OpenCV, a popular computer vision library. It includes code for face detection, eye tracking, and alerting the driver when drowsiness is detected.
- `drowsiness-yoloR`: This folder contains code to detect drowsiness using YOLO-R, a state-of-the-art object detection algorithm with high accuracy on GPUs. It includes pre-trained models and scripts to run the detector on images and videos.


## Demo

To see the models in action, check out the following demos:

### Drowsiness Detection with OpenCV

![Drowsiness Detection Animation](./Animation%20(gif).gif)

### Drowsiness Detection with YOLO-R

![Drowsiness Detection Output GIF](./output.gif)

## Drowsiness Detection Video

<video width="640" height="360" controls>
  <source src="./Drowsiness_1_trim.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>



We hope this repository will be helpful for anyone interested in drowsiness detection and computer vision. Please feel free to contribute or share any feedback!

