## Objective

The objective of this project is to detect the potential of a person falling asleep behind the wheel by detecting whether the driver is awake or drowsy.

## Methodologies

The following methodologies are used for drowsiness detection:
- Face Landmark Detection (FLD): This approach involves detecting the facial landmarks and tracking the eye movements to detect drowsiness.
- Convolutional Neural Networks (CNNs) + Long Short-Term Memory (LSTMs): This approach involves training a deep learning model to detect drowsiness based on eye images.
- Object Detection: This approach involves detecting the driver's face and eyes using object detection algorithms such as YOLOv4-R.

## Logic

The logic for drowsiness detection is as follows:
1. Start the program.
2. Input the video feed from the camera.
3. Use YOLOv4-R to detect the driver's face and eyes.
4. Use the FLD and/or CNN+LSTM models to detect drowsiness based on the eye movements.
5. Display the result indicating whether the driver is awake or drowsy.

# Installation

## Requirements

    Python 3
    OpenCV
    PyTorch

## Installing Dependencies

If you have a CUDA(GPU) already installed on your machine, run the following command:

```bash
# If you have a GPU
pip install -r requirements-gpu.txt


# If you don't have a GPU
pip install -r requirements-cpu.txt
```


## Execution

### For Video Input

To run the code with a video as input, use the following command:
```
python detect_DS_count_base.py --source Drowsiness/videos/Drowsiness_1_trim.mp4 --output Drowsiness/output_videos --names Drowsiness/names_Drowsiness.names --cfg Drowsiness/yolor_p6_Drowsiness.cfg --weights Drowsiness/best_Drowsiness.pt --conf 0.5 --img-size 1280 --device 0 --view-img
```

For real-time detection using the camera, run:
```
python detect_DS_count_base.py --source 0 --names Drowsiness/names_Drowsiness.names --cfg Drowsiness/yolor_p6_Drowsiness.cfg --weights Drowsiness/best_Drowsiness.pt --conf 0.5  --device 0 
```

Note: You can adjust the value of the confidence threshold (conf) and the size of the input image (img-size) according to your needs.

## Demo

To see the models in action, check out the following demos:
### Drowsiness Detection with YOLO-R

![Drowsiness Detection Output GIF](./output.gif)

## Drowsiness Detection Video

<video width="640" height="360" controls>
  <source src="./Drowsiness_1_trim.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>
